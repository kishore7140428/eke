pipeline {
    agent any
    environment {
        REPO_RELEASE_VERSION = '*/${releaseVersion}'
        SONAR_PRO_KEY = 'AWS-EKE'
        SONAR_PRO_VERSION = '1.0.1'
        CHECKSUM =''
        AWS_ACCOUNT_ID = "253145356370"
        AWS_REGION = "us-east-1" 
        REPOSITORY_URI = "253145356370.dkr.ecr.us-east-1.amazonaws.com/awseke"
    }
    stages {
        stage('Checkout') {
            steps {
                checkout scmGit(branches: [[name: '*/main']], extensions: [], userRemoteConfigs: [[credentialsId: 'GitLab', url: 'https://gitlab.com/kishore7140428/eke.git']])
            }
        }
        stage('Build') {
            tools {
                jdk 'java-8'
            }
            steps {
                sh 'chmod -R 755 .'
                sh './gradlew clean -Pversion=${releaseVersion} build'
            }
        }
        stage ('Scan') {
            tools {
                jdk 'java-8'
            }
            steps {
                withSonarQubeEnv(installationName: 'Sonar_12', credentialsId: 'sonar') {
                sh "./gradlew sonarqube \
                  -Dsonar.projectKey=$SONAR_PRO_KEY \
                  -Dsonar.projectName=$SONAR_PRO_KEY \
                  -Dsonar.projectVersion=$SONAR_PRO_VERSION"
                }
             }
        }
        stage('Docker Build') {
            steps { 
                    echo 'Building docker Image'
                    sh "docker build --build-arg RELEASE_VERSION=${releaseVersion} -t awseke:${releaseVersion} ."
                }
        }
stage('Generate k0s Deployment YAML') {
            steps {
                script {
                    def deploymentYamlContent = """
apiVersion: apps/v1
kind: Deployment
metadata:
  name: application
spec:
  replicas: 1
  selector:
    matchLabels:
      app: application
  template:
    metadata:
      labels:
        app: application
    spec:
      containers:
      - name: application
        image: awseke:${releaseVersion}
        ports:
        - containerPort: 2903
"""
                    writeFile file: 'k0s-deployment.yaml', text: deploymentYamlContent
                }
            }
        }
        stage('Generate k0s Service YAML') {
            steps {
                script {
                    def serviceYamlContent = """
apiVersion: v1
kind: Service
metadata:
  name: application-service
spec:
  selector:
    app: application-app
  ports:
    - protocol: TCP
      port: 8082
      targetPort: 8082
  type: NodePort
"""
                    writeFile file: 'k0s-service.yaml', text: serviceYamlContent
                }
            }
        }
    }
post {
        success {
            sh 'docker logout'
            archiveArtifacts artifacts: 'k0s-deployment.yaml', onlyIfSuccessful: true
            archiveArtifacts artifacts: 'k0s-service.yaml', onlyIfSuccessful: true
        }
    }
}
}
