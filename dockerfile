FROM openjdk:17
ADD build/libs/gradleSampleArt-1.0.1-plain.jar gradleSampleArt-1.0.1.jar
EXPOSE 2903
ENTRYPOINT ["java", "-jar","gradleSampleArt-1.0.1.jar"]
